from OpenGL.GLUT import *
from direction import *
from block import Block
import math

class Pacman(object):
    looking_direction = Direction.RIGHT
    walking_direction = Direction.STOP
    position          = Position(12, 7)
    lives             = 3
    open_mouth        = 2.0
    increase_by       = 0.03
    added_value       = -1 * increase_by
    frame_rate        = 5 # per milliseconds (every 15 ms)
    looking_angle = {
        Direction.RIGHT: 25,
        Direction.LEFT: 25+180,
        Direction.UP: 115,
        Direction.DOWN: 115+180,
    }

    @staticmethod
    def bootstrap():
        looking_direction = Direction.RIGHT
        walking_direction = Direction.STOP
        position          = Position(12, 7)
        lives             = 1
        open_mouth        = 2.0
        increase_by       = 0.03
        added_value       = -1 * increase_by
        frame_rate        = 5 # per milliseconds (every 15 ms)
        looking_angle = {
            Direction.RIGHT: 25,
            Direction.LEFT: 25+180,
            Direction.UP: 115,
            Direction.DOWN: 115+180,
        }

    @staticmethod
    def die():
        Pacman.position = Position(12, 7)
        Pacman.lives -= 1
        Pacman.looking_direction = Direction.RIGHT
        Pacman.walking_direction = Direction.STOP


    @staticmethod
    def update_pacman_mouth(is_pause=False):
        if is_pause:
            return 

        if Pacman.open_mouth >= 2:
            Pacman.added_value = -1 * Pacman.increase_by
        elif Pacman.open_mouth <= 1.55:
            Pacman.added_value = Pacman.increase_by

        Pacman.open_mouth += Pacman.added_value

    @staticmethod
    def update_looking_direction(key):
        Pacman.looking_direction = Direction(key)
        Pacman.walking_direction = Direction(key)
        

    @staticmethod
    def move(BoardObj, extra_steps=False):
        if BoardObj.pause:
            return

        added_value = 3 if extra_steps else 1 

        if Pacman.walking_direction == Direction.RIGHT:

            if Pacman.position.x+1 == BoardObj.columns:
                Pacman.position.x = 0
            elif BoardObj.allowed_area[Pacman.position.x + added_value][Pacman.position.y]:
                Pacman.position.x += added_value

        elif Pacman.walking_direction == Direction.LEFT:

            if Pacman.position.x - 1 == -1:
                Pacman.position.x = BoardObj.columns - 1
            elif BoardObj.allowed_area[Pacman.position.x - added_value][Pacman.position.y]:
                Pacman.position.x -= 1

        elif Pacman.walking_direction == Direction.UP:

            if BoardObj.allowed_area[Pacman.position.x][Pacman.position.y + added_value]:
                Pacman.position.y += added_value
                if Pacman.position.y == BoardObj.rows - 1 - 1:
                    Pacman.position.y = 2

        elif Pacman.walking_direction == Direction.DOWN:

            if BoardObj.allowed_area[Pacman.position.x][Pacman.position.y - added_value]:
                Pacman.position.y -= 1
                if Pacman.position.y == 1:
                    Pacman.position.y = BoardObj.rows - 1 - 2

        if BoardObj.maze[Pacman.position.x][Pacman.position.y] == Block.FOOD:
            BoardObj.maze[Pacman.position.x][Pacman.position.y] = Block.SPACE
            BoardObj.score += 1
            BoardObj.all_crumbs -= 1

        for g in BoardObj.ghosts:
            if g.position.x == Pacman.position.x and g.position.y == Pacman.position.y:
                Pacman.die()
