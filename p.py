from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys, math


Game = {
    'width': 600, 'height': 600,
    'board': [],
    'pause': False
}
Ball = {
    'position': {
        'x': Game['width']/2,
        'y': Game['height']/2
    },
    'speed': 2,
    'size': 8,
    'direction': 'stop'
}

def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(Game['width'], Game['height'])
    glutCreateWindow('lab 1')

    glClearColor(0.0, 0.0, 0.0, 0)
    glMatrixMode(GL_PROJECTION)
    gluOrtho2D(0.0, Game['width'], 0.0, Game['height']);

    init_game_board()

    glutDisplayFunc(draw);
    glutTimerFunc(1, update, 0);
    glutKeyboardFunc(handle_char_keys)
    glutSpecialUpFunc(handle_arrows_keys)

    glutMainLoop();
    return

def init_game_board():
    Game['board'] = [[True for i in range(Game['height'])] for j in range(Game['width'])]


def handle_char_keys(key, x, y):
    if ord(key) == 27 or key == 'q':
        sys.exit(0)

    if key == ' ':
        Game['pause'] = ~ Game['pause']

def handle_arrows_keys(key, x, y):
    if key == 100:
        Ball['direction'] = 'left'
    elif key == 101:
        Ball['direction'] = 'up'
    elif key == 102:
        Ball['direction'] = 'right'
    elif key == 103:
        Ball['direction'] = 'down'

def drawRect(x, y, width, height, color=(1.0, 1.0, 1.0), block=True):
    glColor3f(color[0], color[1], color[2])
    glBegin(GL_QUADS)
    glVertex2f(x, y)
    glVertex2f(x + width, y)
    glVertex2f(x + width, y + height)
    glVertex2f(x, y + height)
    glEnd()

    if block:
        for i in range(x-Ball['size']-1, x+width):
            for j in range(y-Ball['size']-1, y+height):
                Game['board'][i][j] = False
            

def update_ball():
    if Game['pause']:
        return
        
    x = Ball['position']['x']
    y = Ball['position']['y']
    # print x, y, len(Game['board']), len(Game['board'][0])

    if Ball['direction'] == 'stop':
        return
    elif Ball['direction'] == 'left':
        if Game['board'][x-1][y]:
            Ball['position']['x'] -= Ball['speed']
            if Ball['position']['x'] <= 0:
                Ball['position']['x'] = Game['width']
    elif Ball['direction'] == 'up':
        if Game['board'][x][y+1]:
            Ball['position']['y'] += Ball['speed']
            if Ball['position']['y'] >= Game['height']:
                Ball['position']['y'] = 0
    elif Ball['direction'] == 'right':
        if Game['board'][x+1][y]:
            Ball['position']['x'] += Ball['speed'] 
            if Ball['position']['x'] >= Game['width']:
                Ball['position']['x'] = 0
    elif Ball['direction'] == 'down':
        if Game['board'][x][y-1]:
            Ball['position']['y'] -= Ball['speed']
            if Ball['position']['y'] <= 0:
                Ball['position']['y'] = Game['height']

        

def update(value):
    update_ball()

    glutPostRedisplay()
    glutTimerFunc(5, update, 0)

def draw():
    glClear(GL_COLOR_BUFFER_BIT); # Clear display window

    drawRect(100, 100, 80, 10)
    drawRect(100, 100, 10, 80)

    

    # ball
    drawRect(Ball['position']['x'], Ball['position']['y'], Ball['size'], Ball['size'], color=(0, 0, 1), block=False)
    
    
    glFlush(); # Process all OpenGL routines

if __name__ == '__main__': main()