from enum import Enum

class Block(Enum):
    FOOD   = 1
    WALL   = 2
    SPACE  = 3
    PACMAN = 4
        
