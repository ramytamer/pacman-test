from OpenGL.GLUT import * 
from OpenGL.GLU import *
from OpenGL.GL import *
import sys, math

G = {
    'width': 400, 'height': 400
}

Pacman = []

def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(G['width'], G['height'])
    glutCreateWindow('test')
    glEnable(GL_BLEND);

    glClearColor(0.0, 0.0, 0.0, 0)
    glMatrixMode(GL_PROJECTION)
    gluOrtho2D(0.0, G['width'], 0.0, G['height']);

    glutDisplayFunc(draw);
    glutTimerFunc(1, update, 0);
    glutKeyboardFunc(handle_char_keys)
    # glutSpecialUpFunc(handle_arrows_keys)

    glutMainLoop();
    return

global small
small = True

def handle_char_keys(key, x, y):
    print key
    if key == 's':
        global small
        small = ~small

i = 1
def update(value):
    glutPostRedisplay()
    glutTimerFunc(1, update, 0)

def drawWall(WallXCo1, WallYCo1, WallYCo2, WallXCo2):
    glColor3f(1, 0, 1);
    glBegin(GL_POLYGON);
    glVertex2f((WallXCo1),(WallYCo1));
    glVertex2f((WallXCo2),(WallYCo1));
    glVertex2f((WallXCo2),(WallYCo2));
    glVertex2f((WallXCo1),(WallYCo2));
    glEnd();

def draw():
    glClear(GL_COLOR_BUFFER_BIT); # Clear display window
    
    # Array for columns    
    wallX = [ 25, 15, 35, 55, 45, 45, 65, 15, 65, 55, 45, 35, 25 ]
    # Array for what part of the columns are coloured, each pair is a line
    wallY = [ 10, 25, 15, 45, 15, 35, 15, 45, 15, 25, 35, 55, 35, 45, 55, 75, 55, 75, 65, 75, 65, 85, 75, 85, 95, 105 ]
    WallXCo = 0;
    WallYCo = 0;
    for pi in xrange(0, 13):
        WallXCo1 = wallX[WallXCo] - 1.25;
        WallXCo2 = wallX[WallXCo] + 1.25;
        WallYCo1 = (wallY[WallYCo] - 1.25) * -1;
        WallYCo2 = (wallY[WallYCo + 1] + 1.25) * -1;
        drawWall(WallXCo1, WallYCo1, WallXCo2, WallYCo2);
        WallXCo = WallXCo + 1;
        WallYCo = WallYCo + 2;


    glFlush(); # Process all OpenGL routines

if __name__ == '__main__': main()