from cx_Freeze import setup, Executable
from shutil import copyfile

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(
    packages = [
        "OpenGL",
        "sys",
        "math",
        "pprint",
        "enum",
        "numpy",
        "random",
        "time",
        "json"
    ], 
    excludes = [])

base = 'Console'

executables = [
    Executable('main.py', base=base, targetName = 'pacman-game')
]

setup(name='pacman',
      version = '1.0',
      description = 'pacman in opengl',
      options = dict(build_exe = buildOptions),
      executables = executables)

copyfile("data.json", "build/exe.linux-x86_64-2.7/")
copyfile("bricks.bmp", "build/exe.linux-x86_64-2.7/")