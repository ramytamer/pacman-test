from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys, math
from pprint import pprint
from board import Board
from block import Block
from drawer import Drawer
from pacman import Pacman
from ghost import Ghost

BoardObj = Board()
TIMER = 0.0

def draw():
    glClear(GL_COLOR_BUFFER_BIT)
    display_func()
    glutSwapBuffers()


def display_func():
    glClear(GL_COLOR_BUFFER_BIT)
    Drawer.draw_maze(BoardObj)
    Drawer.draw_score(BoardObj)
    Drawer.draw_help(BoardObj)
    Drawer.draw_lives(BoardObj)
    Drawer.draw_pacman(Pacman.position.x, Pacman.position.y)

    for g in BoardObj.ghosts:
        Drawer.draw_ghost(g.position.x, g.position.y, g.color)
        if g.position.x == Pacman.position.x and g.position.y == Pacman.position.y:
            Pacman.die()

    if Pacman.lives == 0:
        BoardObj.gamever = BoardObj.pause = True
        Drawer.gameover(BoardObj)
    elif BoardObj.all_crumbs == 0:
        BoardObj.gamever = BoardObj.pause = True
        Drawer.wingame(BoardObj)

    # print Pacman.position, BoardObj.allowed_area[Pacman.position.x+1, Pacman.position.y]
    # print Pacman.position.x, Pacman.position.y
    # print BoardObj.allowed_area[Pacman.position.x+1][Pacman.position.y], \
    #       BoardObj.maze[Pacman.position.x+1][Pacman.position.y], Pacman.position.x+1
    # print BoardObj.allowed_area[Pacman.position.x+2][Pacman.position.y], \
    #       BoardObj.maze[Pacman.position.x+2][Pacman.position.y], Pacman.position.x+2
    # print BoardObj.allowed_area[Pacman.position.x+3][Pacman.position.y], \
    #       BoardObj.maze[Pacman.position.x+3][Pacman.position.y], Pacman.position.x+3
    # print BoardObj.allowed_area[Pacman.position.x+4][Pacman.position.y], \
    #       BoardObj.maze[Pacman.position.x+4][Pacman.position.y], Pacman.position.x+4
    # sys.exit(0)
    glutSwapBuffers()

def idle_func():
   global TIMER
   TIMER += 0.000025;
   if TIMER > 1.0:
       for g in BoardObj.ghosts:
           g.move(BoardObj)
       TIMER = 0
       display_func()

def handle_char_keys(key, x, y):
    if key == 'q' or ord(key) == 27:
        sys.exit(0)

    if key == ' ' and not BoardObj.gameover:
        print 'here'
        BoardObj.pause = True if not BoardObj.pause else False
    elif key == 'k':
        Pacman.lives += 1

def handle_arrows_keys(key, x, y):
    if key < 100 or key > 103:
        return

    Pacman.update_looking_direction(key)
    Pacman.move(BoardObj)
    

def update_func(value):
    Pacman.update_pacman_mouth(BoardObj.pause)
    glutTimerFunc(Pacman.frame_rate, update_func, 0)

def move_pacman(value):
    Pacman.move(BoardObj)
    if value:
        glutTimerFunc(50, move_pacman, 0)

def main():
    try:
        BoardObj.bootstrap_from_file('data.json')

        glutInit(sys.argv)
        glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH)
        glutInitWindowSize(BoardObj.win_width, BoardObj.win_height)
        glutCreateWindow('Pacman Game')

        glClearColor(0.0, 0.0, 0.0, 0)
        glMatrixMode(GL_PROJECTION)
        gluOrtho2D(0.0, BoardObj.columns, 0.0, BoardObj.rows)

        # Callbacks
        glutDisplayFunc(draw)
        glutIdleFunc(idle_func)
        glutTimerFunc(Pacman.frame_rate, update_func, 0)
        # glutTimerFunc(50, move_pacman, 0)
        glutKeyboardFunc(handle_char_keys)
        glutSpecialFunc(handle_arrows_keys)

        # display_func()

        glutMainLoop()
    except Exception as e:
        sys.exit(e)

    return

if __name__ == '__main__': main()