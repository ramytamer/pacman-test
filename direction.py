from enum import Enum

class Direction(Enum):
    STOP  = 0
    LEFT  = 100
    UP    = 101
    RIGHT = 102
    DOWN  = 103
    RUN   = 112

class Position(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "({0}, {1})".format(self.x, self.y)
        