from OpenGL.GLUT import * 
from OpenGL.GLU import *
from OpenGL.GL import *
import sys, math

G = {
    'width': 400, 'height': 400
}
Angle = 115
OpenMouth = 2.0
AddedValue = -0.05

Pacman = []

def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(G['width'], G['height'])
    glutCreateWindow('test')
    glEnable(GL_BLEND);

    glClearColor(0.0, 0.0, 0.0, 0)
    glMatrixMode(GL_PROJECTION)
    gluOrtho2D(0.0, 5, 0.0, 5);

    glutDisplayFunc(draw);
    glutTimerFunc(30, update, 0);
    glutKeyboardFunc(handle_char_keys)
    # glutSpecialUpFunc(handle_arrows_keys)

    glutMainLoop();
    return

def handle_char_keys(key, x, y):
    if key == 'q' or ord(key) == 27:
        sys.exit(0)
    else:
        global Angle
        Angle = Angle+1 if Angle <= 150 else 0
        print Angle


def draw_char():
    x = 2.5
    y = 2.5
    radius = 0.3;
    glColor3f(1.0, 1.0, 0);

    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(float(x) + 0.5, float(y) + 0.5);
    angle = 1.0
    while angle < 361.0:
        x2 = x + math.sin(angle) * radius + 0.5;
        y2 = y + math.cos(angle) * radius + 0.5;
        glVertex2f(x2, y2);
        angle += 0.2
    
    glEnd();

def update(value):
    global OpenMouth
    global AddedValue
    if OpenMouth >= 2:
        AddedValue = -0.03
    elif OpenMouth <= 1.55:
        AddedValue = 0.03

    OpenMouth += AddedValue

    glutPostRedisplay()
    glutTimerFunc(15, update, 0)

def draw_pacman(x, y, radius=1, color=(1, 1, 0)):
    triangleAmount = 20
    
    twicePi = OpenMouth * math.pi;
    
    i = 0
    glColor3f(*color)

    glPushMatrix()
    glTranslatef(x, y, 0)
    # left: 25
    # right: 25+180
    # up: 115
    # down: 115+180
    glRotatef(115+180, 0, 0, 1)
    glTranslatef(-1 * x, -1 * y, 0)

    glBegin(GL_TRIANGLE_FAN)
    glVertex2f(x, y);
    while i <= triangleAmount:
        glVertex2f(
            x + (radius * math.cos(i * twicePi / triangleAmount)),
            y + (radius * math.sin(i * twicePi / triangleAmount))
        )
        i += 1
    glEnd();

    glPopMatrix()

def hallow_circle(x, y, radius=1, color=(1.0, 1.0, 1.0)):
    lineAmount = 100; # of triangles used to draw circle
    
    twicePi = 2.0 * math.pi;
    
    i = 0
    glColor3f(*color)
    glBegin(GL_LINE_LOOP);
    while i < lineAmount:
        glVertex2f(
            x + (radius * math.cos(i *  twicePi / lineAmount)), 
            y + (radius* math.sin(i * twicePi / lineAmount))
        );
        i += 1
    glEnd();

def filled_circle(x, y, radius=1, color=(1.0, 1.0, 1.0)):
    triangleAmount = 20
    
    twicePi = 2.0 * math.pi;
    
    i = 0
    glColor3f(*color)
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(x, y);
    while i <= triangleAmount:
        glVertex2f(
            x + (radius * math.cos(i *  twicePi / triangleAmount)), 
            y + (radius * math.sin(i * twicePi / triangleAmount))
        );
        i += 1
    glEnd();

def draw_crumb(x, y, radius=1, color1=(0, 0, 1), color2=(1, 0, 0.5)):
    hallow_circle(x, y, radius, color1)
    filled_circle(x, y, radius - 0.2, color2)

def draw_ghost(x, y, body_color=(0.96, 0.25, 0.25)):
    width = 0.5
    height = 0.25
    head_radius = width/2.0
    leg_radius = width/6.0
    eye_offset = width/4.0

    glColor3f(*body_color)
    glRectf(x, y, x+width, y+height)

    filled_circle(x+(width/2.0), y+height, radius=head_radius, color=body_color)

    filled_circle(x+leg_radius, y, radius=leg_radius, color=body_color)
    filled_circle(x+3*leg_radius, y, radius=leg_radius, color=body_color)
    filled_circle(x+width-leg_radius, y, radius=leg_radius, color=body_color)

    filled_circle(x+eye_offset, y+height, radius=leg_radius, color=(0.96, 0.61, 0.61))
    filled_circle(x+width-eye_offset, y+height, radius=leg_radius, color=(0.96, 0.61, 0.61))
    
def draw():
    try:
        glClear(GL_COLOR_BUFFER_BIT); # Clear display window
            
        # draw_crumb(2.5, 2.5) 
        # draw_pacman(2.5, 4)
        # filled_circle(2.5, 4)
        
        draw_ghost(2, 2)

        glFlush(); # Process all OpenGL routines
    except Exception as e:
        print e
        sys.exit(0)
    

if __name__ == '__main__': main()