from block import Block
from pprint import pprint
import numpy as np
import random, sys
from ghost import Ghost
from direction import Direction
import json
from drawer import Drawer

class Board(object):
    def __init__(self, rows=25, columns=25, win_width=700, win_height=700):
        self.rows         = rows
        self.columns      = columns
        self.win_width    = win_width
        self.win_height   = win_height
        self.maze         = np.empty([rows, columns], dtype=Block)
        self.allowed_area = np.full([rows, columns], False, dtype=bool)
        self.pause        = False
        self.is_drawn     = False
        self.score        = 0
        self.all_crumbs   = 0
        self.gameover     = False
        self.ghosts       = [
            Ghost(1, 10, 15, (0.96, 0.25, 0.25), Direction.LEFT),
            Ghost(2, 11, 15, (0.25, 0.96, 0.25), Direction.RIGHT),
            Ghost(3, 12, 15, (0.25, 0.25, 0.96), Direction.UP),
            Ghost(4, 13, 15, (1, 1, 1), Direction.DOWN),
        ]
    
    def bootstrap(self):
        for i, row in enumerate(self.maze):
            for j, cell in enumerate(row):
                if i==0 or i==self.rows-1 or j==0 or j==self.columns-1:
                    self.maze[i, j] = Block.WALL
                else:
                    self.maze[i, j] = random.choice([Block.FOOD, Block.WALL, Block.FOOD])

                self.all_crumbs += 1 if self.maze[i, j] == Block.FOOD else 0
                self.allowed_area[i, j] = True if self.maze[i, j] != Block.WALL else False

    def bootstrap_from_file(self, file_path):
        try:
            f = open(file_path, 'r')
        except Exception as e:
            raise e

        data = json.loads(f.read())
        self.rows = data['rows']
        self.columns = data['columns']
        self.win_width = data['win_width']
        self.win_height = data['win_height']
        Drawer.texture_file = data['texture_file']
        for i in range(0, self.rows):
            for j in range(0, self.columns):
                cell = data['maze'][i][j]
                self.maze[j, i] = Block(cell)
                self.allowed_area[j, i] = True if Block(cell) != Block.WALL else False
                self.all_crumbs += 1 if Block(cell) == Block.FOOD else 0

        self.maze = self.maze[...,::-1]
        self.allowed_area = self.allowed_area[...,::-1]