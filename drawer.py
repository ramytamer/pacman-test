from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import math
from pacman import Pacman
from block import Block
import numpy as np
import Image
import sys

class Drawer(object):
    texture = None
    texture_file = 'bricks.bmp'

    @staticmethod
    def draw_space(x, y):
        glColor3f(0, 0, 0)
        glRectf(x, y, x + 1, y + 1)

    @staticmethod
    def prepare_texture():
        texture = glGenTextures(1)

        glBindTexture( GL_TEXTURE_2D, texture )

        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT )
        glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT )

        try:
            img = Image.open(Drawer.texture_file)
        except Exception as e:
            print 'No texture file !'
            sys.exit(-1)

        img_data = np.array(list(img.getdata()), np.uint8)
        gluBuild2DMipmaps( GL_TEXTURE_2D, 3, img.size[0], img.size[1], GL_RGB, GL_UNSIGNED_BYTE, img_data )

        return texture

    @staticmethod
    def draw_wall(x, y):
        # texture = 5
        
        if Drawer.texture == None:
            Drawer.texture = Drawer.prepare_texture()

        glEnable( GL_TEXTURE_2D )
        glBindTexture( GL_TEXTURE_2D, Drawer.texture )
        
        # glColor3f(0.133, 0.32, 0.61)
        glColor3f(1, 1, 0.5)

        glBegin(GL_QUADS)
        glTexCoord2d(x, y);      glVertex2f(x, y)
        glTexCoord2d(x+1, y);    glVertex2f(x+1, y)
        glTexCoord2d(x+1, y+1);  glVertex2f(x+1, y+1)
        glTexCoord2d(x, y+1);    glVertex2f(x, y+1)
        glEnd()
        # import sys
        # sys.exit(0)

        # glRectf(x, y, x+1, y+1)

    @staticmethod
    def draw_food(x, y, radius=0.2, color1=(0, 0, 1), color2=(1, 0, 0.5)):
        Drawer.hallow_circle(x+0.5, y+0.5, radius, color1)
        Drawer.filled_circle(x+0.5, y+0.5, radius - 0.09, color=(0.74, 0.59, 0.84))
        # draw_crumb(x, y, color1=(0.87, 0.043, 0.807))
        # glColor3f(0.87, 0.043, 0.807)
        # # glRectf(x+0.5, y+0.5, x+0.75, y+0.75)
        # glBegin(GL_TRIANGLE_FAN)
        # radius = 0.1
        # glVertex2f(float(x)+0.5, float(y)+0.5)
        # angle = 1.0
        # while angle < 361.0:
        #     x2 = x + math.sin(angle) * radius + 0.5
        #     y2 = y + math.cos(angle) * radius + 0.5
        #     glVertex2f(x2, y2)
        #     angle += 0.2
        
        # glEnd()


    @staticmethod
    def filled_circle(x, y, radius=1.0, color=(1.0, 1.0, 1.0)):
        triangleAmount = 20
        
        twicePi = 2.0 * math.pi;
        
        i = 0
        glColor3f(*color)
        glBegin(GL_TRIANGLE_FAN);
        glVertex2f(x, y);
        while i <= triangleAmount:
            glVertex2f(
                x + (radius * math.cos(i *  twicePi / triangleAmount)), 
                y + (radius * math.sin(i * twicePi / triangleAmount))
            );
            i += 1
        glEnd();

    @staticmethod
    def hallow_circle(x, y, radius=1.0, color=(1.0, 1.0, 1.0)):
        lineAmount = 100; # of triangles used to draw circle
        
        twicePi = 2.0 * math.pi;
        
        i = 0
        glColor3f(*color)
        glBegin(GL_LINE_LOOP);
        while i < lineAmount:
            glVertex2f(
                x + (radius * math.cos(i *  twicePi / lineAmount)), 
                y + (radius* math.sin(i * twicePi / lineAmount))
            );
            i += 1
        glEnd();

    # @staticmethod
    # def draw_ghost(x, y, color=(0, 1, 0)):
    #     glColor3f(*color)
    #     glBegin(GL_TRIANGLES)
    #     glVertex2f(x, y+0.25)
    #     glVertex2f(x+1, y+0.25)
    #     glVertex2f(x+0.5, y+0.75)
    #     glEnd()

    @staticmethod
    def draw_ghost(x, y, body_color=(0.96, 0.25, 0.25)):
        x += 0.25
        y += 0.25

        width = 0.5
        height = 0.25
        head_radius = width/2.0
        leg_radius = width/6.0
        eye_offset = width/4.0
        eye_color = (
            0, 0, 0
        )

        glColor3f(*body_color)
        glRectf(x, y, x+width, y+height)

        Drawer.filled_circle(x+(width/2.0), y+height, radius=head_radius, color=body_color)

        Drawer.filled_circle(x+leg_radius, y, radius=leg_radius, color=body_color)
        # Drawer.filled_circle(x+3*leg_radius, y, radius=leg_radius, color=body_color)
        Drawer.filled_circle(x+width-leg_radius, y, radius=leg_radius, color=body_color)

        Drawer.filled_circle(x+eye_offset, y+height, radius=leg_radius, color=eye_color)
        Drawer.filled_circle(x+width-eye_offset, y+height, radius=leg_radius, color=eye_color)
        

    @staticmethod
    def draw_pacman(x, y, radius=0.4, color=(1, 1, 0)):
        # Pacman.position.x, Pacman.position.y =  [x, y]

        x += 0.5; y += 0.5

        triangleAmount = 20
        
        twicePi = Pacman.open_mouth * math.pi;
        
        i = 0
        glColor3f(*color)

        glPushMatrix()
        glTranslatef(x, y, 0)
        # left: 25
        # right: 25+180
        # up: 115
        # down: 115+180
        glRotatef(Pacman.looking_angle[Pacman.looking_direction], 0, 0, 1)
        glTranslatef(-1 * x, -1 * y, 0)

        glBegin(GL_TRIANGLE_FAN)
        glVertex2f(x, y);
        while i <= triangleAmount:
            glVertex2f(
                x + (radius * math.cos(i * twicePi / triangleAmount)),
                y + (radius * math.sin(i * twicePi / triangleAmount))
            )
            i += 1
        glEnd();

        glPopMatrix()

    @staticmethod
    def draw_lives(BoardObj):
        lives = "Lives: " + str(Pacman.lives)
        Drawer.draw_text(lives, BoardObj.columns - 5, BoardObj.rows - 1, BoardObj.rows, BoardObj.columns)

    @staticmethod
    def draw_score(BoardObj):
        score_txt = "Score: " + str(BoardObj.score)
        Drawer.draw_text(score_txt, 2, BoardObj.rows - 1, BoardObj.rows, BoardObj.columns)


    @staticmethod
    def draw_help(BoardObj):
        Drawer.draw_text("Q: quit", 2, 1, BoardObj.rows, BoardObj.columns, (0, 0, 1), GLUT_BITMAP_HELVETICA_18)
        Drawer.draw_text("SPACE: pause/start", BoardObj.columns - 8, 1, BoardObj.rows, BoardObj.columns, (0, 0, 1), GLUT_BITMAP_HELVETICA_18)

    @staticmethod
    def draw_text(txt, x, y, width, height, color=(1, 1, 1), font=GLUT_BITMAP_HELVETICA_18):
        glColor3f(*color)
        glMatrixMode(GL_PROJECTION)
        matrix = glGetDouble( GL_PROJECTION_MATRIX )
        
        glLoadIdentity()
        glOrtho(0.0, width, 0.0, height, -1.0, 1.0)
        glMatrixMode(GL_MODELVIEW)
        glPushMatrix()
        glLoadIdentity()
        glRasterPos2i(x, y)

        for character in txt:
            glutBitmapCharacter(font, ord(character))

        glPopMatrix()
        glMatrixMode(GL_PROJECTION)
        glLoadMatrixd( matrix ) 
        glMatrixMode(GL_MODELVIEW)

    @staticmethod
    def draw_maze(BoardObj):
        for i in reversed(range(0, BoardObj.rows)):
            for j in range(0, BoardObj.columns):
                if BoardObj.maze[i][j] == Block.SPACE:
                    Drawer.draw_space(i, j)
                elif BoardObj.maze[i][j] == Block.WALL:
                    Drawer.draw_wall(i, j)
                elif BoardObj.maze[i][j] == Block.FOOD:
                    Drawer.draw_food(i, j, color1=(0.87, 0.043, 0.807))

    @staticmethod
    def gameover(BoardObj):
        for i in reversed(range(0, BoardObj.rows)):
            for j in range(0, BoardObj.columns):
                Drawer.draw_space(i, j)

        Drawer.draw_score(BoardObj)
        Drawer.draw_lives(BoardObj)

        x             = int(math.ceil(BoardObj.rows / 2.0)) - 3
        y             = int(math.ceil(BoardObj.columns / 2.0)) + 2
        
        ramy_txt      = "RAMY TAMER              2162"
        haybat_txt    = "HOSSAM HAYBAT      2208"
        press_q       = "Press Q to quit"

        Drawer.draw_text("GAME OVER", x, y, BoardObj.rows, BoardObj.columns, (1, 0, 0))

        Drawer.draw_text(ramy_txt, x-2, y - 2, BoardObj.rows, BoardObj.columns, (1, 1, 0))
        Drawer.draw_text(haybat_txt, x-2, y - 4, BoardObj.rows, BoardObj.columns, (1, 1, 0))
        
        Drawer.draw_text(press_q, x, y-7, BoardObj.rows, BoardObj.columns, (1, 0, 1))

    @staticmethod
    def wingame(BoardObj):
        for i in reversed(range(0, BoardObj.rows)):
            for j in range(0, BoardObj.columns):
                Drawer.draw_space(i, j)

        Drawer.draw_score(BoardObj)
        Drawer.draw_lives(BoardObj)

        x             = int(math.ceil(BoardObj.rows / 2.0)) - 3
        y             = int(math.ceil(BoardObj.columns / 2.0)) + 2
        
        ramy_txt      = "RAMY TAMER              2162"
        haybat_txt    = "HOSSAM HAYBAT      2208"
        press_q       = "Press Q to quit"

        Drawer.draw_text("YOU WON !", x+1, y, BoardObj.rows, BoardObj.columns, (0, 1, 0))

        Drawer.draw_text(ramy_txt, x-2, y - 2, BoardObj.rows, BoardObj.columns, (1, 1, 0))
        Drawer.draw_text(haybat_txt, x-2, y - 4, BoardObj.rows, BoardObj.columns, (1, 1, 0))
        
        Drawer.draw_text(press_q, x, y-7, BoardObj.rows, BoardObj.columns, (1, 0, 1))
