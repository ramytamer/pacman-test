from direction import *
from pacman import Pacman
import random, time

class Ghost(object):
    def __init__(self, ghost_id, x, y, color, default_direction):
        self.ghost_id          = ghost_id
        self.position          = Position(x, y)
        self.color             = color
        self.at_inital         = True
        self.steps             = 0
        self.walking_direction = default_direction
        self.default_direction = default_direction
        self.done_left = self.done_right = self.done_up = self.done_down = False
        random.seed(time.time())

    def can_move_right(self, BoardObj, added_value):
        return self.position.x+1 == BoardObj.columns or BoardObj.allowed_area[self.position.x + added_value][self.position.y]
    
    def can_move_left(self, BoardObj, added_value):
        return self.position.x-1 == -1 or BoardObj.allowed_area[self.position.x - added_value][self.position.y]
    
    def can_move_up(self, BoardObj, added_value):
        return self.position.y == BoardObj.rows-2 or BoardObj.allowed_area[self.position.x][self.position.y + added_value]

    def can_move_down(self, BoardObj, added_value):
        return self.position.y == 1 or BoardObj.allowed_area[self.position.x][self.position.y - added_value]

    def move_right(self, BoardObj, added_value):
        self.position.x = 0 if self.position.x+1 == BoardObj.columns else self.position.x+added_value

    def move_left(self, BoardObj, added_value):
        self.position.x = BoardObj.columns-1 if self.position.x-1 == -1 else self.position.x-added_value

    def move_up(self, BoardObj, added_value):
        self.position.y = 2 if self.position.y == BoardObj.rows-2 else self.position.y+added_value

    def move_down(self, BoardObj, added_value):
        self.position.y = BoardObj.rows - 1 - 2 if self.position.y == 1 else self.position.y-added_value

    def move(self, BoardObj):
        if BoardObj.pause:
            return

        if self.at_inital:
            self.position.y += 2
            self.at_inital = False

        added_value = 1

        if self.steps == 6:
            if self.walking_direction == Direction.RIGHT:
                self.walking_direction = random.choice([Direction.RIGHT, Direction.UP, Direction.DOWN])
            elif self.walking_direction == Direction.LEFT:
                self.walking_direction = random.choice([Direction.LEFT, Direction.UP, Direction.DOWN])
            elif self.walking_direction == Direction.UP:
                self.walking_direction = random.choice([Direction.RIGHT, Direction.LEFT, Direction.UP])
            elif self.walking_direction == Direction.DOWN:
                self.walking_direction = random.choice([Direction.RIGHT, Direction.LEFT, Direction.DOWN])

            self.steps = 0
        else:
            self.steps += 1
        
        # To make it slower
        if self.steps % 2 == 0: return

        if self.walking_direction == Direction.RIGHT and self.can_move_right(BoardObj, added_value):
            self.move_right(BoardObj, added_value)
        elif self.walking_direction == Direction.LEFT and self.can_move_left(BoardObj, added_value):
            self.move_left(BoardObj, added_value)
        elif self.walking_direction == Direction.UP and self.can_move_up(BoardObj, added_value):
            self.move_up(BoardObj, added_value)
        elif self.walking_direction == Direction.DOWN and self.can_move_down(BoardObj, added_value):
            self.move_down(BoardObj, added_value)
        else:
            self.steps = 6